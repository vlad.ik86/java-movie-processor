package movies.importer;

/**
 * The Movie class is an object that stores movie related information.
 * @author Vladyslav Berezhnyak
 *
 */
public class Movie 
{
	private String title;
	private String releaseYear;
	private String runTime;
	private String src;
	
	/**
	 * This constructor sets the values for the private fields above.
	 * @param The title of the movie
	 * @param The release year of the movie
	 * @param The run time of the movie
	 * @param The information source for the movie
	 */
	public Movie(String title, String releaseYear, String runTime, String src)
	{
		this.title = title;
		this.releaseYear = releaseYear;
		this.runTime = runTime;
		this.src = src;
	}
	
	/**
	 * This method returns the title.
	 * @return title of the movie
	 */
	public String getTitle()
	{
		return this.title;
	}
	
	/**
	 * This method returns the release year.
	 * @return this.releaseYear
	 */
	public String getReleaseYear()
	{
		return this.releaseYear;
	}
	
	/**
	 * This method returns the run time.
	 * @return run time of the movie
	 */
	public String getRunTime()
	{
		return this.runTime;
	}
	
	/**
	 * This method returns the information source of the movie.
	 * @return Source name
	 */
	public String getSrc()
	{
		return this.src;
	}
	
	/**
	 * This method overrides the toString method and returns the toString() equivalent of the Movie object.
	 * @return String of the movie object
	 */
	@Override
	public String toString()
	{
		StringBuilder s = new StringBuilder();
		String tab = "\t";
		
		s.append(getTitle() + tab + getReleaseYear() + tab + getRunTime() + tab + getSrc());
		
		return s.toString();
	}
	
	/**
	 * This method overrides the equals method in order to properly compare if two movies are equal
	 * by following this rule: "Two movies are considered the same if they have exactly the same title 
	 * and release year and the runtime between the two is no more than 5 minutes apart."
	 * @return True or false
	 */
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Movie)
		{
			try
			{
				Movie mo = (Movie) o;
				int durThis = Integer.parseInt(this.getRunTime());
				int durMo = Integer.parseInt(mo.getRunTime());
				int diff = Math.abs((durThis - durMo));
				
				if(mo.getTitle().equals(this.getTitle()) && mo.getReleaseYear().equals(this.getReleaseYear()) && diff <= 5)
				{
					return true;
				}
			}
			
			catch(NumberFormatException e)
			{
				return false;
			}
		}
		
		return false;
	}
}