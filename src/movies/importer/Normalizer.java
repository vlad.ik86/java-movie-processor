package movies.importer;
import java.util.ArrayList;

/**
 * The Normalizer class takes as input a ArrayList<String> that has been processed to have only the title, release date, runtime, and source,
 * and returns it as an ArrayList<String> full of normalized, easier to use Movies. 
 * Normalized Movies have all lowercase titles, and only a single word in the runtime field.
 * @author Luke Weaver
 */
public class Normalizer extends Processor{
	public Normalizer(String sourceDir, String outputDir){
		super(sourceDir, outputDir, false);
	}
	
	/**
	 * The toLowercase method takes as input String s, and returns it in all lowercase
	 * @param the String you want in lowercase
	 * @return the String in lowercase
	 */
	public static String toLowercase(String s) {
		String word = "";
		
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
				word += (char)(s.charAt(i) + 32);
			}else {
				word += s.charAt(i);
			}
		}
		return word;
	}
	
	/**
	 * The firstWord method takes as input String s, and returns only the first word of it
	 * @param the word you want only the first word of
	 * @return the first word
	 */
	public static String firstWord(String s) {
		if(s.contains(" ")) {
			String[] words = s.split(" ", 0);
			return words[0];
		}
		return s;
	}
	
	/**
	 * The process method takes as input the processed ArrayList<String> arr, and returns it as a normalized Movie with a
	 * title in full lowercase, and only a single word in the runtime field.
	 * @param the ArrayList<String> of Movies to normalize
	 * @return the processed ArrayList<String> of normalized Movies
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> arr){
		ArrayList<String> newArr = new ArrayList<String>();
		String[] column;
		
		for(int i = 0; i < arr.size(); i++) {
			column = arr.get(i).split("\t", -1);
			
			//title, release date, runtime, and source
			Movie m = new Movie(toLowercase(column[0]), column[1], firstWord(column[2]), column[3]);
			newArr.add(m.toString());
		}
		
		return newArr;
	}
}
