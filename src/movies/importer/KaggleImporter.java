package movies.importer;
import java.util.ArrayList;

/**
 * The ImdbImporter class reads through a provided 
 * @author Luke Weaver
 */
public class KaggleImporter extends Processor{
	/**
	 * This constructor takes as input the source directory and the output directory, then calls super
	 * from the Processor, since KaggleImporter extends it. srcContainsHeader is set to true, which removes the first line.
	 * @param The source directory
	 * @param The output directory
	 */
	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	/**
	 * This method takes as input the ArrayList<String> representing the kaggle file and makes it into 
	 * a new ArrayList<String> containing .toString() from Movie objects.
	 * @param Raw ArrayList<String>
	 * @return Standardized ArrayList<String>
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> arr){
		ArrayList<String> newArr = new ArrayList<String>();
		String[] column;
		
		for(String row : arr) {
			column = row.split("\t", -1);
			
			/*column 1) cast 1, 2) cast 2, 3) cast 3, 4) cast 4, 5) cast 5, 
			6) cast 6, 7) description, 8) director 1, 9) director 2, 10) director 3,
			11) genre, 12) rating, 13) release, 14) runtime, 15) studio,
			16) title, 17) writer 1, 18) writer 2, 19) writer 3, 20) year*/
			
			//movie only needs title (16), year (20), runtime (14), and source ("kaggle")
			if(column.length == 21) {
				Movie m = new Movie(column[15], column[20], column[13], "kaggle");
				newArr.add(m.toString());
			}
		}
		
		return newArr;
	}
}
