package movies.importer;
import java.util.ArrayList;

/**
 * The Validator class reads through the ArrayList from a processed .txt file, validates
 * if the Arraylist contains valid movies and produces a new ArrayList with valid movies.
 * @author Vladyslav Berezhnyak
 */
public class Validator extends Processor
{
	/**
	 * This constructor takes as input the source directory and the output directory, then makes a call to
	 * the super from the Processor class. Sets srcContainsHeader to false, keeping the first line of the file.
	 * @param The source directory
	 * @param The output directory
	 */
	public Validator(String sourceDir, String outputDir) 
	{
		super(sourceDir, outputDir, false);
	}

	/**
	 * This method takes as input the ArrayList<String> containing text from a processed .txt file and produces 
	 * a new ArrayList<String> containing validated movies.
	 * @param Processed ArrayList<String> 
	 * @return Processed ArrayList<String> with valid movies
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) 
	{
		ArrayList<String> newList = input;
		String[] column;
				
		for(int i = 0; i < input.size(); i++)
		{
			column = input.get(i).split("\t", -1);
			
			// column[0] = title(NOT ORIGINAL_TITLE)	column[1] = year	column[2] = duration	column[3] = "Imdb"
			if(!(validMovie(column) && possibleConversionToNumber(column[1]) && possibleConversionToNumber(column[2])))
			{
				newList.remove(input.get(i));
			}
		}
		
		return newList;
	}
	
	/**
	 * This method validates whether each field from a row isn't null or an empty string.
	 * @param String[] of fields from the row in question 
	 * @return True or false
	 */
	private boolean validMovie(String[] column)
	{
		for(String s : column)
		{
			if(s != null && s.length() != 0)
			{
				continue;
			}
			
			else
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * This method validates if the year and duration fields can be converted to a number
	 * @param String that needs to be converted to a number
	 * @return True or false
	 */
	private boolean possibleConversionToNumber(String column)
	{
		try
		{
			int x = Integer.parseInt(column);
		}
		
		catch(NumberFormatException e)
		{
			return false;
		}
		
		return true;
	}
}
