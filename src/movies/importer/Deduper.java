package movies.importer;
import java.util.ArrayList;

/**
 * The Deduper class reads through the ArrayList from a processed .txt file, validates
 * if there are no duplicate movies and produces a new ArrayList with no duplicates.
 * @author Vladyslav Berezhnyak
 *
 */
public class Deduper extends Processor
{
	/**
	 * This constructor takes as input the source directory and the output directory, then makes a call to
	 * the super from the Processor class. Sets srcContainsHeader to false, keeping the first line of the file.
	 * @param The source directory
	 * @param The output directory
	 */
	public Deduper(String sourceDir, String outputDir) 
	{
		super(sourceDir, outputDir, false);
	}

	/**
	 * This method takes as input the ArrayList<String> containing text from a processed .txt file, converts
	 * it to a ArrayList<Movie>, validates if there are any duplicates and adds non duplicates to another ArrayList<Movie>.
	 * In the end, the last ArrayList<Movie> is converted back into a ArrayList<String>, which is returned by this program.
	 * @param Processed ArrayList<String> 
	 * @return Processed ArrayList<String> with no duplicate movies
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) 
	{
		ArrayList<Movie> moList = new ArrayList<Movie>();
		String[] column;
		
		for(String row : input)
		{
			column = row.split("\t", -1);
			
			// column[0] = title(NOT ORIGINAL_TITLE)	column[1] = year	column[2] = duration	column[3] = "Imdb"
			if(column.length == 4)
			{
				Movie mo = new Movie(column[0], column[1], column[2], column[3]);
				moList.add(mo);
			}
		}
		
		ArrayList<Movie> noDupes = new ArrayList<Movie>();
		for(Movie moA : moList)
		{
			//.contains uses an overriden .equals to compare if two movies are the same found in the Movie class
			if(!noDupes.contains(moA))
			{
				noDupes.add(moA);
			}
			
			else
			{
				int index = noDupes.indexOf(moA);
				Movie original = noDupes.get(index);
				
				String originalTitle = original.getTitle();
				String originalYear = original.getReleaseYear();
				String originalRunTime = original.getRunTime();
				
				String srcOriginal = original.getSrc();
				String srcCurrent = moA.getSrc();
				String srcFinal = new String();
				
				if(!srcOriginal.equals(srcCurrent) && !(srcOriginal.equals("Imdb;kaggle") || srcOriginal.equals("kaggle;Imdb")))
				{
					srcFinal = srcOriginal + ";" + srcCurrent;
				}
				
				else
				{
					srcFinal = srcOriginal;
				}
				
				Movie finalMo = new Movie(originalTitle, originalYear, originalRunTime, srcFinal);
				noDupes.set(index, finalMo);
			}
		}
		
		ArrayList<String> newList = new ArrayList<String>();
		for(Movie m : noDupes)
		{
			newList.add(m.toString());
		}
		
		return newList;
	}
}