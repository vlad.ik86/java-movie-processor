package movies.importer;
import java.io.IOException;

/**
 * The ImporterPipeline program executes all of the processors in sequences to yield a fully processed .txt file.
 * @author Luke Weaver
 * @author Vladyslav Berezhnyak
 *
 */
public class ImportPipeline {
	
	/**
	 * The main method creates a Processor[] and stores all of the processes. It then invokes the processAll method.
	 * @author Luke Weaver
	 * @param Args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		ImdbImporter iI = new ImdbImporter("app-execution\\input-Imdb", "app-execution\\imported");
		KaggleImporter kI = new KaggleImporter("app-execution\\input-Kaggle", "app-execution\\imported");
		Normalizer n = new Normalizer("app-execution\\imported", "app-execution\\normalized");
		Validator v = new Validator("app-execution\\normalized", "app-execution\\validated");
		Deduper d = new Deduper("app-execution\\validated", "app-execution");
		
		Processor[] procs = {iI, kI, n, v, d};
		
		processAll(procs);
	}
	
	/**
	 * The processAll method takes as input a Processor[] and invokes the execute and printProcess methods for each Processor.
	 * It also calculates how long it takes for the entire program to execute and process everything in seconds and milliseconds.
	 * @author Luke Weaver
	 * @author Vladyslav Berezhnyak
	 * @param Processor[] of Processors
	 * @throws IOException
	 */
	private static void processAll(Processor[] procs) throws IOException {
		final int toMillis = 1000000;
		final int toSeconds = 1000;
		long timeStart = System.nanoTime() / toMillis;
		for(Processor p : procs) {
			p.execute();
			printProcess(p);
		}
		
		long timeEnd = System.nanoTime() / toMillis;
		long durationMillis = (timeEnd - timeStart);
		long durationSeconds = durationMillis / toSeconds;
		
		System.out.println("Program executed in " + durationSeconds + " seconds (" + durationMillis + " milliseconds)");
	}
	
	/**
	 * The printProcess method determines which kind of Process was executed and prints its name after its executed.
	 * @author Vladyslav Berezhnyak
	 * @param Process object
	 */
	private static void printProcess(Object o)
	{
		if(o instanceof ImdbImporter)
		{
			System.out.println("ImdbImporter executed");
		}
		
		if(o instanceof KaggleImporter)
		{
			System.out.println("KaggleImporter executed");
		}
		
		if(o instanceof Normalizer)
		{
			System.out.println("Normalizer executed");
		}
		
		if(o instanceof Validator)
		{
			System.out.println("Validator executed");
		}
		
		if(o instanceof Deduper)
		{
			System.out.println("Deduper executed");
		}
	}
}
