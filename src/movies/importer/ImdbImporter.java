package movies.importer;
import java.util.ArrayList;

/**
 * The ImdbImporter class reads through the ArrayList from a raw Imdb .txt file and produces
 * a new ArrayList to match the .toString() of the Movie class.
 * @author Vladyslav Berezhnyak
 *
 */
public class ImdbImporter extends Processor
{
	/**
	 * This constructor takes as input the source directory and the output directory, then makes a call to
	 * the super from the Processor class. Sets srcContainsHeader to true, removing the first line of the file.
	 * @param The source directory
	 * @param The output directory
	 */
	public ImdbImporter(String sourceDir, String outputDir) 
	{
		super(sourceDir, outputDir, true);
	}

	/**
	 * This method takes as input the ArrayList<String> containing text from the Imdb file and produces 
	 * a new ArrayList<String> containing .toString() from Movie objects.
	 * @param Raw ArrayList<String>
	 * @return Processed ArrayList<String> with standardized format
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) 
	{
		ArrayList<String> newList = new ArrayList<String>();
		String[] column;
		
		for(String row : input)
		{
			column = row.split("\t", -1);
			
			// column[2] = title(NOT ORIGINAL_TITLE)	column[3] = year	column[6] = duration
			if(column.length == 22)
			{
				Movie mo = new Movie(column[2], column[3], column[6], "Imdb");
				newList.add(mo.toString());
			}
		}
		
		return newList;
	}
}
