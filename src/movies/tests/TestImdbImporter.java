package movies.tests;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import movies.importer.ImdbImporter;

/**
 * The TestImdbImporter JUnit Test Case runs various tests on the ImdbImporter class.
 * @author Vladyslav Berezhnyak
 *
 */
class TestImdbImporter 
{
	/**
	 * Tests if the ImdbImporter class processes "ImdbSmallFile.txt" and outputs the toString() format of the Movie class into a new file.
	 * @throws IOException
	 */
	@Test
	void processImdb() throws IOException
	{
		String inputDir = "C:\\Users\\Vlad\\Documents\\School\\Dawson\\CompSci\\Semester III\\Programming III\\Labs\\Project 1\\MovieTest\\input";
		String outputDir = "C:\\Users\\Vlad\\Documents\\School\\Dawson\\CompSci\\Semester III\\Programming III\\Labs\\Project 1\\MovieTest\\output";
		
		ImdbImporter proc = new ImdbImporter(inputDir, outputDir);
		proc.execute();
	}
}
