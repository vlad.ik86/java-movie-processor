package movies.tests;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import movies.importer.Validator;

/**
 * The TestValidator JUnit Test Case runs various tests on the Validator class.
 * @author Vladyslav Berezhnyak
 *
 */
class TestValidator 
{
	/**
	 * Tests if the Validator class processes the output from a modified ImdbImporter class with false information 
	 * and outputs a new text file containing valid movies.
	 * @throws IOException
	 */
	@Test
	void processValidator() throws IOException 
	{
		String inputDir = "C:\\Users\\Vlad\\Documents\\School\\Dawson\\CompSci\\Semester III\\Programming III\\Labs\\Project 1\\MovieTest\\output";
		String outputDir = "C:\\Users\\Vlad\\Documents\\School\\Dawson\\CompSci\\Semester III\\Programming III\\Labs\\Project 1\\MovieTest\\outputValidator";
		
		Validator proc = new Validator(inputDir, outputDir);
		proc.execute();
	}
}