package movies.tests;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import movies.importer.*;

/**
 * The TestMovie JUnit Test Case runs various tests on the Movie Class.
 * @author Vladyslav Berezhnyak
 *
 */
class TestMovie 
{
	/**
	 * Tests if a Movie object from the Movie class is created and prints out the object's toString().
	 */
	@Test
	void createMovie() 
	{
		Movie mo = new Movie("Harry Potter and The Philosopher's Stone", "2001", "120", "Imdb");
		
		assertEquals(mo.getTitle(), "Harry Potter and The Philosopher's Stone");
		assertEquals(mo.getReleaseYear(), "2001");
		assertEquals(mo.getRunTime(), "120");
		assertEquals(mo.getSrc(), "Imdb");
		
		System.out.println(mo);
	}
}