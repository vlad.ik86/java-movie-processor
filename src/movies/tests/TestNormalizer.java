package movies.tests;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Normalizer;
import movies.importer.Validator;

/**
 * The TestNormalizer JUnit Test Case runs various tests on the Normalizer class.
 * @author Luke Weaver
 */
class TestNormalizer {
	/**
	 * Tests if the Normalizer class toLowercase method works properly, and doesn't return any uppercase letters
	 */
	@Test
	public void testToLowercase() {
		assertEquals(Normalizer.toLowercase("HoWdY, ThIs Is A TeSt"), "howdy, this is a test");
	}
	
	/**
	 * Tests if the Normalizer class firstWord method works properly, and returns only one word
	 */
	@Test
	public void testFirstWord() {
		assertEquals(Normalizer.firstWord("hi is the first word"), "hi");
	}
	
	/**
	 * Tests if the Normalizer class process method works properly, and returns processed Movies
	 */
	@Test
	public void testNormalizerProcess() {
		Normalizer n = new Normalizer("C:\\Users\\Luke\\Downloads\\Project_1_MovieImporter\\input", "C:\\Users\\Luke\\Downloads\\Project_1_MovieImporter\\output");
		ArrayList<String> arr = new ArrayList<String>();
		arr.add("TITLE\t2020\t128 minutes\tkaggle");
		arr.add("TITLE2\t2021\t130 minutes\tkaggle");
		
		ArrayList<String> testArr = new ArrayList<String>();
		testArr.add("title\t2020\t128\tkaggle");
		testArr.add("title2\t2021\t130\tkaggle");
		
		assertEquals(n.process(arr), testArr);
	}
}
