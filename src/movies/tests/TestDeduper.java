package movies.tests;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import movies.importer.Deduper;

/**
 * The TestDeduper JUnit Test Case runs various tests on the Deduper class.
 * @author Vladyslav Berezhnyak
 *
 */
class TestDeduper 
{
	/**
	 * Tests if the Deduper class processes the output from the ImdbImporter with hardwritten false information 
	 * and outputs a new text file containing no duplicates.
	 * @throws IOException
	 */
	@Test
	void processDeduper() throws IOException 
	{
		String inputDir = "C:\\Users\\Vlad\\Documents\\School\\Dawson\\CompSci\\Semester III\\Programming III\\Labs\\Project 1\\MovieTest\\output";
		String outputDir = "C:\\Users\\Vlad\\Documents\\School\\Dawson\\CompSci\\Semester III\\Programming III\\Labs\\Project 1\\MovieTest\\outputDeduper";
		
		Deduper proc = new Deduper(inputDir, outputDir);
		proc.execute();
	}
}