package movies.tests;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import movies.importer.KaggleImporter;

/**
 * The TestKaggleImporter JUnit Test Case runs various tests for the KaggleImporter class.
 * @author Luke Weaver
 */

class TestKaggleImporter {
	/**
	 * Tests if the KaggleImporter class processes "KaggleSmallFile.txt" and outputs the toString() format of the Movie class into a new file.
	 * @throws IOException
	 */
	@Test
	void processKaggle() throws IOException
	{
		String inputDir = "C:\\Users\\Luke\\Downloads\\Project_1_MovieImporter\\input";
		String outputDir = "C:\\Users\\Luke\\Downloads\\Project_1_MovieImporter\\output";
		
		KaggleImporter proc = new KaggleImporter(inputDir, outputDir);
		proc.execute();
	}
}
